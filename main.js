document.getElementById('task-form').addEventListener('submit', function(event) {
  event.preventDefault();

  // Get the task input value
  var taskInput = document.getElementById('task-input');
  var taskValue = taskInput.value.trim();

  // Check if the input value is not empty
  if (taskValue !== '') {
    // Create a new list item
    var li = document.createElement('li');
    li.className = 'flex justify-between items-center bg-white p-4 mb-2 rounded-lg';

    // Create the task text
    var taskText = document.createElement('span');
    taskText.textContent = taskValue;
    taskText.className = 'mr-2';

    // Create the delete button
    var deleteButton = document.createElement('button');
    deleteButton.innerHTML = '<i class="fas fa-trash-alt"></i>';
    deleteButton.className = 'text-red-500';

    // Append the task text and delete button to the list item
    li.appendChild(taskText);
    li.appendChild(deleteButton);

    // Append the list item to the task list
    var taskList = document.getElementById('task-list');
    taskList.appendChild(li);

    // Clear the task input value
    taskInput.value = '';
  }
});

// Event delegation for the delete button
document.getElementById('task-list').addEventListener('click', function(event) {
  if (event.target.parentElement.nodeName === 'BUTTON') {
    event.target.parentElement.parentElement.remove();
  }
});
